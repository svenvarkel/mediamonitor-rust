# Welcome to MediaMonitor

This small Rust program pulls song titles and artists from a special URL and stores this info in MongoDB database.

# Building the program

 1. Install Rust -> https://www.rust-lang.org/tools/install
 
 1. Pull source code (this repo)
 
        git clone https://svenvarkel@bitbucket.org/svenvarkel/mediamonitor-rust.git
 
 1. Run cargo:
  
        cargo build --release
        
    That should create the program in target/release folder. Enjoy! :) 
  
# Running the program

    ./mediamonitor --help
    
    mediamonitor 0.1.0

    USAGE:
        mediamonitor <station> <destination>
    
    FLAGS:
        -h, --help       Prints help information
        -V, --version    Prints version information
    
    ARGS:
        <station>
        <destination>
        
Export MONGODB_URI environment variable that is used to create MongoDB connection.

Specify station code (e.g ROCKFM for Rock FM or STAAR for Rock FM Metal) and a destination database name. There are other stations, too
but figure these out yourself ;) 


# Building for Linux while being on Mac

Docker is used for this feature. Try this command line:

    docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/mediamonitor -w /usr/src/mediamonitor rust:latest cargo build --release
    
A Linux-compatible binary should land in target/release folder after the command has finished successfully.