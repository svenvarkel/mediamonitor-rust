///
/// This program is created by Sven Varkel <sven@prototypely.com>
///
mod date_format;

use crate::date_format::my_date_format;

use std::env;
use rand::Rng;
use reqwest::Error;
use structopt::StructOpt;
use tokio::runtime::Runtime;
use chrono::{DateTime, Utc, Local};
use serde::{Serialize, Deserialize};
use mongodb::bson::{doc, DateTime as dt};
use mongodb::{Client, options::ClientOptions, error, Database, options::UpdateOptions};

#[derive(Serialize, Deserialize, Debug)]
pub struct Play {
    StationName: String,
    SongHistoryList: Vec<Track>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Track {
    Album: String,
    Artist: String,
    Title: String,
    IsSkippable: bool,
    TimeStamp: i32,
    #[serde(with = "my_date_format")]
    SqlTimeStamp: DateTime<Local>,
    Count: i32,
}

#[derive(StructOpt)]
struct Cli {
    station: String,
    destination: String,
}

///
/// This is the main interface to the program
///
#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Cli::from_args();

    let mut rng = rand::thread_rng();
    let timestamp: DateTime<Utc> = Utc::now();
    let rnd: i32 = rng.gen::<i32>();
    let request_url = format!("https://dad.akaver.com/api/songtitles/{}?rnd={}&_={}", &args.station, rnd, timestamp.timestamp_millis());

    println!("getting {:?}", request_url);
    let response = reqwest::get(&request_url).await?;
    let status = response.status();
    let play: Play = response.json().await?;
    let tracks: Vec<Track> = play.SongHistoryList;
    save_records_to_db(args.destination.to_string(), tracks).await;
    return Ok(());
}

///
/// This function creates database connection
///
async fn get_db() -> Result<Database, mongodb::error::Error> {
    let client_uri =
        env::var("MONGODB_URI").expect("You must set the MONGODB_URI environment var!");

    let mut client_options = ClientOptions::parse(client_uri.as_ref()).await?;
    client_options.app_name = Some("mediamonitor".to_string());
    let client = Client::with_options(client_options)?;
    let db = client.database(&"rockfm".to_string());
    return Ok(db);
}

///
/// This function saves records to mongodb collection
///
async fn save_records_to_db(collection: String, tracks: Vec<Track>) -> Result<(), error::Error> {
    let db = get_db().await?;
    let collection = db.collection(&collection);
    for track in tracks {
        let s: DateTime<Utc> = DateTime::from(track.SqlTimeStamp);
        let d = bson::DateTime(s);

        let filter = doc! {
            "artist": &track.Artist,
            "title": &track.Title,
            "timestamp": &track.TimeStamp
        };
        let update = doc! {
            "$set":{
                "artist": &track.Artist,
                "title": &track.Title,
                "timestamp": &track.TimeStamp,
                "datetime": bson::to_bson(&d).unwrap()
            },
            "$setOnInsert":{
                "createdAt": Utc::now()
            },
            "$currentDate":{
                "updatedAt":true
            }
        };
        let options = mongodb::options::UpdateOptions::builder().upsert(true).build();
        let response = collection.update_one(filter, update, options).await?;
    }
    return Ok(());
}
